<?php
    $this->title = $page->title;
    $this->description = $page->description;
    $this->keywords = $page->keywords;
?>
<!-- Header -->
<div class="header" id="header">
    <div class="container">

        <!-- Menu -->
        <div class="menu">

            <div class="menu_container">
                <ul class="items">
                    <li><a href="/">Главная</a></li>
                    <li><a href="/#products">Наша продукция</a></li>
                    <li><a href="#contact">Контакты</a></li>

                    <ul class="tel">
                        <li>
                            <a href="tel:+79038684255">+7 903 868 42 55</a>
                        </li>
                    </ul>
                </ul>

                <!-- Social Links -->
                <ul class="social">
                    <li><a href="https://vk.com/torty_milya" target="_blank" class="vk">vkontakte</a></li>
                </ul>
                <!-- Social Links Ends! -->
            </div>

        </div>
        <!-- Menu Ends -->

        <div class="h1"><a href="/">Milya <span>Cakes</span></a></div>

    </div>
</div>
<!-- Header Ends! -->

<!-- Page Content -->
<div class="page">
    <div class="container">

        <div class="clear-box"></div>

        <!-- Abstract -->
        <div class="abstract">
            <a class="anchor" id="about"></a>
            <h1>Праздничная выпечка</h1>
            <p><?= $page->content; ?></p>

        </div>
        <!-- Abstract Ends! -->




        <!-- Featured Products -->
        <div class="myproducts">

            <?php foreach($goods as $good):?>
                <!-- Product -->
                <div class="product">
                    <div class="img">
                        <img src="<?= $good->getImage(); ?>" alt="prod_1.jpg">
                        <a href="<?= $good->getImage(); ?>"></a>
                    </div>
                    <div class="clear"></div>
                        <div class="description">
                            <span class="h3"><?= $good->name; ?></span>
                            <p><?= $good->description; ?></p>
                        </div>
                </div>
                <!-- Product Ends! -->
            <?php endforeach;?>

            <div class="callback">
                <a href="#callback" class="callback_button">Отправить заявку</a>
            </div>
            <div></div>

        </div>
        <!-- Featured Products Ends! -->

        <!-- Map -->
        <div class="map">
            <a class="anchor" id="contact"></a>
            <div id="map_canvas"></div>

            <div class="address">
                <div>
                    <span class="h3">Контакты</span>
                    <span class="h4">Milya Cake's</span>
                    <p>г. Брянск<br><strong>Телефон:<a href="tel:+79038684255"> +7 903 868 4255 </a></strong><br><a href="https://vk.com/torty_milya" target="_blank">https://vk.com/torty_milya</a></p>
                </div>
            </div>
        </div>
            <!-- Map Ends! -->

            <!-- Contact -->
            <div class="contact">


                <!-- Contact Form -->
                <div class="form" id="callback">
                    <p>Используйте эту форму для связи с нами по любым вопросам.</p>
                    <form action="" method="post" class="ajax_form af_example">

                        <div class="form-group">

                            <div class="controls">
                                <input type="text" id="af_name" name="name" value="" placeholder="Ваше имя" class="form-control"/>
                            </div>
                        </div>

                        <div class="form-group">

                            <div class="controls">
                                <input type="email" id="email" name="email" value="" placeholder="Ваша почта" class="form-control"/>
                            </div>
                        </div>

                        <div class="form-group">

                            <div class="controls">
                                <input type="phone" id="phone" name="phone" value="" placeholder="Ваш телефон" class="form-control"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="controls">
                                <textarea id="message" name="message" class="form-control" rows="5" placeholder="Ваше сообщение"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="controls">
                                <button type="submit" class="btn btn-primary">Отправить</button>
                            </div>
                        </div>
                    </form>
                </div>
                <!-- Contact Form Ends! -->

                <!-- More -->
                <div class="more">
                    <div class="h4">Хотите знать больше?</div>
                    <p>Пожалуйста, задавайте все интересующие вопросы нам! Заполните форму слева и мы обязательно свяжемся с вами!<br>
                        Также связывайтесь с нами Вконтакте через личные сообщения!</p>

                    <ul>
                        <li>Юбилеи</li>
                        <li>Дни рождения</li>
                        <li>Подарки любимым</li>
                        <li>Детские праздники</li>
                        <li>Свадьбы</li>
                    </ul>
                </div>
                <!-- More Ends! -->

            </div>
            <!-- Contact Ends! -->

        </div>
    </div>
    <!-- Page Ends! -->

    <!-- Footer -->
    <div class="footer">
        <div class="container">
            <p>Copyright &copy;2017 <a href="/">MiLYA CAKES</a> - <?= $page->footer; ?></p>
        </div>
    </div>
    <!-- Footer Ends! -->

