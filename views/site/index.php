<?php

/* @var $this yii\web\View */

$this->title = $page->title;
$this->description = $page->description;
$this->keywords = $page->keywords;
?>
<div class="header" id="header">
    <div class="container">

        <!-- Menu -->
        <div class="menu">

            <div class="menu_container">
                <ul class="items">
                    <li><a href="#about">О нас</a></li>
                    <li><a href="#products">Наша продукция</a></li>
                    <li><a href="#contact">Контакты</a></li>

                    <ul class="tel">
                        <li>
                            <a class="tel" href="tel:+79038684255">+7 903 868 42 55</a>
                        </li>
                    </ul>
                </ul>

                <!-- Social Links -->
                <ul class="social">
                    <li><a href="https://vk.com/torty_milya" target="_blank" class="vk">vkontakte</a></li>
                </ul>
                <!-- Social Links Ends! -->
            </div>

        </div>
        <!-- Menu Ends -->

        <div class="h1"><a>Milya <span>Cakes</span></a></div>

    </div>
</div>
<!-- Header Ends! -->

<!-- Page Content -->
<div class="page">
    <div class="container">

        <!-- Slider -->
        <div class="promo">

            <div class="flexslider">
                <ul class="slides">
                    <li><img src="/public/images/slides/02.jpg" alt="" /></li>
                    <li><img src="/public/images/slides/03.jpg" alt="" /></li>
                    <li><img src="/public/images/slides/04.jpg" alt="" /></li>
                    <li><img src="/public/images/slides/05.jpg" alt="" /></li>
                    <li><img src="/public/images/slides/06.jpg" alt="" /></li>
                    <li><img src="/public/images/slides/07.jpg" alt="" /></li>
                    <li><img src="/public/images/slides/08.jpg" alt="" /></li>
                </ul>
            </div>

        </div>
        <!-- Slider Ends! -->

        <!-- Abstract -->
        <div class="abstract">
            <a class="anchor" id="about"></a>
            <h1><?= $page->header; ?></h1>
            <p>Здесь описать про доставку, указав номер телефона и адрес электронной почты, условия доставки и время доставки.<br>
                MiLYA CaKES - современная домашняя выпечка с использованием только натуральных продуктов! <br>
                Разнообразие ассортимента и вкуса - всё только лучшее для Вас!</p>
            <h2>Заголовок второго уровня - тоже важный</h2>

            <p><em>Мы создаем уникальные, ароматные и такие домашние торты для ваших мероприятий, а наши цены приятно удивят Вас!</em></p>
        </div>
        <!-- Abstract Ends! -->

        <!-- Featured Products -->
        <div class="products">
            <a class="anchor" id="products"></a>
            <div class="h2">Продукция</div>
            <!-- Product -->
            <div class="product">
                <div>
                    <img src="/public/images/prod_1.jpg" alt="prod_1.jpg">
                    <a href="/[[~5]]"></a>
                </div>
                <a href="/[[~5]]"><span class="h3">Готовые изделия</span></a>
                <p>Готовые ароматные торты для душевного семейного вечера или праздника в кругу близких и друзей.</p>
            </div>
            <!-- Product Ends! -->

            <!-- Product -->
            <div class="product">
                <div>
                    <img src="/public/images/prod_2.jpg" alt="prod_2.jpg">
                    <a href="/"></a>
                </div>
                <span class="h3">Торты на заказ</span>
                <p>Уникальные безмастичные торты станут украшением вашего праздничного стола или сладким подарком для любимых.</p>
            </div>
            <!-- Product Ends! -->

            <!-- Product -->
            <div class="product">
                <div>
                    <img src="/public/images/prod_3.jpg" alt="prod_3.jpg">
                    <a href="/"></a>
                </div>
                <span class="h3">Капкейки</span>
                <p>Очаровательные мини-тортики - капкейки станут отличным дополнением к торту на вашем праздничном столе.</p>
            </div>
            <!-- Product Ends! -->



            <!-- Product -->
            <div class="product-left">
                <div>
                    <img src="/public/images/prod_4.jpg" alt="prod_4.jpg">
                    <a href="/"></a>
                </div>
                <span class="h3">Пироги</span>
                <p>Вкуснейшие ароматные пироги с начинками. Именно начинка отличает их от других мучных изделий и делает такими уникальными.</p>
            </div>
            <!-- Product Ends! -->

            <!-- Product -->
            <div class="product-left">
                <div>
                    <img src="/public/images/prod_5.jpg" alt="prod_5.jpg">
                    <a href="/prazdnichnaja-vypechka"></a>
                </div>
                <a href="/prazdnichnaja-vypechka"><span class="h3">Праздничная выпечка</span></a>
                <p>Такая выпечка всегда создает в доме особую атмосферу тепла и уюта, объединяя семью.</p>
            </div>
            <!-- Product Ends! -->


        </div>
        <!-- Featured Products Ends! -->

        <!-- Services -->
        <div class="services">
            <a class="anchor" id="services"></a>
            <span class="h2">Почему мы?</span>
            <p class="description"></p>
            <!-- тут надо описать преимущества работы с нами -->
            <div class="service">
                <span class="h3">Мы заботимся о наших клиентах</span>
                <ul>
                    <li><a href="">идеальный состав из натуральных продуктов</a></li>
                    <li><a href="">уникальная рецептура</a></li>
                    <li><a href="">бесподобный вкус</a></li>
                    <li><a href="">без ГМО и красителей/</a></li>
                    <li><a href="">без мастики (мастика возможна по просьбе клиента и только в качестве небольшого украшения) /</a></li>
                </ul>
            </div>

            <div class="event">
                <span class="h3">Изготавливаем изделия для ваших событий</span>
                <ul>
                    <li><a href="">Юбилеи</a></li>
                    <li><a href="">Дни рождения</a></li>
                    <li><a href="">Подарки любимым</a></li>
                    <li><a href="">Детские праздники</a></li>
                    <li><a href="">Свадьбы</a></li>
                </ul>
            </div>

            <div class="delivery">
                <span class="h3">Доставка</span>
                <p>Осуществляем быструю доставку готовых изделий по городу. Стоимость доставки 100р. Возможен самовывоз.</p>
            </div>
        </div>
        <!-- Services Ends! -->





        <!-- Map -->
        <div class="map">
            <a class="anchor" id="contact"></a>
            <div id="map_canvas"></div>

            <div class="address">
                <div>
                    <span class="h3">Контакты</span>
                    <span class="h4">Milya Cake's</span>
                    <p>г. Брянск<br><strong>Телефон:<a href="tel:+79038684255"> +7 903 868 4255 </a></strong><br><a href="https://vk.com/torty_milya" target="_blank">https://vk.com/torty_milya</a></p>
                </div>
            </div>
        </div>
        <!-- Map Ends! -->

        <!-- Contact -->
        <div class="contact">


            <!-- Contact Form -->
            <div class="form" id="callback">
                <p>Используйте эту форму для связи с нами по любым вопросам.</p>
                <form action="" method="post" class="ajax_form af_example">

                    <div class="form-group">

                        <div class="controls">
                            <input type="text" id="af_name" name="name" value="" placeholder="Ваше имя" class="form-control"/>
                        </div>
                    </div>

                    <div class="form-group">

                        <div class="controls">
                            <input type="email" id="email" name="email" value="" placeholder="Ваша почта" class="form-control"/>
                        </div>
                    </div>

                    <div class="form-group">

                        <div class="controls">
                            <input type="phone" id="phone" name="phone" value="" placeholder="Ваш телефон" class="form-control"/>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="controls">
                            <textarea id="message" name="message" class="form-control" rows="5" placeholder="Ваше сообщение"></textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="controls">
                            <button type="submit" class="btn btn-primary">Отправить</button>
                        </div>
                    </div>
                </form>
            </div>
            <!-- Contact Form Ends! -->

            <!-- More -->
            <div class="more">
                <div class="h4">Хотите знать больше?</div>
                <p>Пожалуйста, задавайте все интересующие вопросы нам! Заполните форму слева и мы обязательно свяжемся с вами!<br>
                    Также связывайтесь с нами Вконтакте через личные сообщения!</p>

                <ul>
                    <li>Юбилеи</li>
                    <li>Дни рождения</li>
                    <li>Подарки любимым</li>
                    <li>Детские праздники</li>
                    <li>Свадьбы</li>
                </ul>
            </div>
            <!-- More Ends! -->

        </div>
        <!-- Contact Ends! -->

    </div>
</div>
<!-- Page Ends! -->

<!-- Footer -->
<div class="footer">
    <div class="container">
        <p>Copyright &copy;2017 <a>MiLYA CAKES</a></p>
    </div>
</div>