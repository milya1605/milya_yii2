<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "good".
 *
 * @property integer $id
 * @property string $name
 * @property integer $category_id
 * @property string $image
 * @property string $description
 *
 * @property Page $category
 */
class Good extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'good';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description'], 'required']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'category_id' => 'Category ID',
            'image' => 'Image',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Page::className(), ['id' => 'category_id']);
    }

    public function saveImage($filename){
        $this->image = $filename;

        return $this->save(false);
    }

    /*
     * Для хостинга раскомментить /web/
     */
    public function getImage(){
        if($this->image){
            //return '/web/uploads/'.$this->image;
            return '/uploads/'.$this->image;
        }
    }
}
