<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "page".
 *
 * @property integer $id
 * @property string $name
 * @property string $title
 * @property string $description
 * @property string $keywords
 * @property string $header
 * @property string $content
 * @property string $footer
 * @property integer $isCategory
 * @property string $image
 *
 * @property Good[] $goods
 */
class Page extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'page';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'title', 'description', 'keywords', 'header', 'content', 'footer', 'image', 'isCategory'], 'required'],
            [['name', 'title', 'description', 'keywords', 'header', 'content', 'footer'], 'string'],
            [['isCategory'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'title' => 'Title',
            'description' => 'Description',
            'keywords' => 'Keywords',
            'header' => 'Header',
            'content' => 'Content',
            'footer' => 'Footer',
            'isCategory' => 'Is Category',
            'image' => 'Image',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoods()
    {
        return $this->hasMany(Good::className(), ['category_id' => 'id']);
    }

    public function saveImage($filename){
        $this->image = $filename;

        return $this->save(false);
    }

    /*
     * для хостинга раскомментить /web/
     */
    public function getImage(){
        if($this->image){
            //return '/web/uploads/'.$this->image;
            return '/uploads/'.$this->image;
        }
    }
}
