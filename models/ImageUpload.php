<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
* 
*/
class ImageUpload extends Model
{
	
	public $image;

	public function rules(){
		return [
			[['image'], 'required'],
			[['image'], 'file', 'extensions'=>'jpg,png']
		];
	}

	public function UploadFile($file, $currentImage){
		
		$this->image = $file;

		if($this->validate()){
			//удаление существующей картинки
			if(file_exists(Yii::getAlias('@web'.'/uploads/'.$currentImage))){
				unlink(Yii::getAlias('@web').'uploads/'.$currentImage);
			}

			$filename = strtolower(md5(uniqid($filename->baseName)).'.'.$file->extension);

			$file->saveAs(Yii::getAlias('@web').'uploads/'.$filename);

			return $filename;
		}
		
	}

}