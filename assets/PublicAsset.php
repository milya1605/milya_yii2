<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class PublicAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'public/style.css',
        'public/callback.css',
        'public/scripts/flexslider/flexslider.css',
    ];
    public $js = [
        '/public/scripts/jquery-1.11.0.min.js',
        '/public/scripts/flexslider/jquery.flexslider-min.js',
        '/public/scripts/imagelightbox.min.js',
        '/public/scripts/functions.js',
        '/public/scripts/form.js',
        '/public/scripts/scroll.js',
    ];
    public $depends = [

    ];
}
