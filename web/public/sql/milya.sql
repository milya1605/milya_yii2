-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Мар 17 2017 г., 23:14
-- Версия сервера: 5.5.50
-- Версия PHP: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `milya`
--

-- --------------------------------------------------------

--
-- Структура таблицы `good`
--

CREATE TABLE IF NOT EXISTS `good` (
  `id` int(8) NOT NULL,
  `name` varchar(255) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `good`
--

INSERT INTO `good` (`id`, `name`, `category_id`, `image`, `description`) VALUES
(3, 'Кулич пасхальный', NULL, '0f653a2bdbc446e0dbefb5e4bebdd468.jpg', 'Готовые ароматные булки для главного христианского праздника. Закажите у нас прекрасную выпечку и порадуйте себя и своих близких.');

-- --------------------------------------------------------

--
-- Структура таблицы `page`
--

CREATE TABLE IF NOT EXISTS `page` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `keywords` varchar(255) NOT NULL,
  `header` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `footer` varchar(255) NOT NULL,
  `isCategory` int(1) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `page`
--

INSERT INTO `page` (`id`, `name`, `title`, `description`, `keywords`, `header`, `content`, `footer`, `isCategory`, `image`) VALUES
(1, 'main', 'Главная страница', 'Описание для главной страницы', 'ключи главной страницы', 'Главная страница', 'Текст главной страницы', 'подвал главной страницы', 0, '0'),
(2, 'prazdnichnaja-vypechka', 'Праздничная выпечка', 'Описание для страницы с праздничной выпечкой', 'ключи для страницы с праздничной выпечкой', 'Праздничная выпечка', 'Отличный способ украсить свой стол в светлый праздник Пасхи сладостями от MiLYA CaKES. Куличи пасхальные домашние станут неотъемлемой частью любого праздничного стола. Заказать оригинальные вкусные куличи на Пасху можно уже сейчас!\r\nКупить праздничную выпечку в Брянске с доставкой можно на нашем сайте. Milya занимается выпечкой куличей очень давно и всегда радует своих клиентов.\r\nЗаказывая куличи на Пасху у нас Вы:\r\n - экономите время;\r\n - экономите энергию:\r\n - получаете вкусные куличи с доставкой на дом.\r\nMiLYA CaKES - современная домашняя праздничная выпечка с использованием только натуральных продуктов! \r\nРазнообразие ассортимента и вкуса - всё только лучшее для Вас! Вкуснейшая праздничная выпечка по выгодным ценам.', 'праздничная выпечка', 1, '07b829005d26d34c3ae4175bd48b9c46.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(8) NOT NULL,
  `name` varchar(255) NOT NULL,
  `login` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `email` varchar(35) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `name`, `login`, `password`, `email`) VALUES
(1, 'kolyha', 'kolyha', '123456', 'kolyha5@yandex.ru');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `good`
--
ALTER TABLE `good`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Индексы таблицы `page`
--
ALTER TABLE `page`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `good`
--
ALTER TABLE `good`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `page`
--
ALTER TABLE `page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `good`
--
ALTER TABLE `good`
  ADD CONSTRAINT `good_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `page` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
